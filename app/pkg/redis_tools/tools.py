import redis


class RedisTools:
    """
    Redis tools class.
    """

    __redis_connect = redis.Redis(host="redis", port=6379)

    @classmethod
    def set_pair(cls, pair: str, price: str) -> None:
        """
        Set pair, price method.
        """
        cls.__redis_connect.set(pair, price)

    @classmethod
    def get_pair(cls, pair: str) -> str:
        """
        Get pair method.
        """
        return cls.__redis_connect.get(pair)

    @classmethod
    def get_keys(cls):
        """
        Get keys list method.
        """
        return cls.__redis_connect.keys(pattern="*")
