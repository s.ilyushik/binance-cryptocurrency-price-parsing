from fastapi import APIRouter


router = APIRouter(prefix="/api/v1")


@router.get("/check")
def user_check():
    """
    Check method.
    """
    return {"check": "passed"}
