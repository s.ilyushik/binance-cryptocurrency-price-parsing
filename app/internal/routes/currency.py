from fastapi import APIRouter, HTTPException, status

from app.pkg.redis_tools.tools import RedisTools


router = APIRouter(prefix="/api/v1/currency", tags=["Pairs"])


@router.get("/{pair}")
def get_currency_pair(pair: str):
    """
    Get currency method.
    """

    if pair not in [s.decode("utf-8") for s in RedisTools.get_keys()]:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return {"pair": pair, "price": RedisTools.get_pair(pair)}
