import os
from dotenv import load_dotenv

import aiohttp

from app.internal.schemas.currency import Symbols
from app.pkg.redis_tools.tools import RedisTools


load_dotenv()


async def on_startup():
    """
    Get all pairs async method.
    """

    async with aiohttp.ClientSession() as session:
        async with session.get(os.getenv("ALL_PAIRS_KEY")) as response:
            response_json = await response.json()
            parsed_pairs = Symbols(**response_json)
            cut_pairs = parsed_pairs.symbols[
                : os.getenv("LIMIT_NUMBER_OF_PAIRS")
            ]
            symbols = [pair.symbol for pair in cut_pairs]

            for symbol in symbols:
                RedisTools.set_pair(symbol, "0")


async def on_loop_startup():
    """
    Method for updating the value of pairs once per n time.
    """

    for symbol in RedisTools.get_keys():
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{os.getenv('CURRENCY_PAIR_KEY')}{symbol.decode('utf-8')}"
            ) as response:
                response_json = await response.json()
                RedisTools.set_pair(symbol, response_json.get("price"))
