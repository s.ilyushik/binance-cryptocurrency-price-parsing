from typing import List

from pydantic import BaseModel


class Symbol(BaseModel):
    """
    Schema class to retrieve the 'symbol' field value.

    From symbols list from all pairs.
    """

    symbol: str


class Symbols(BaseModel):
    """
    Schema class to retrieve the 'symbols' list of all pairs.
    """

    symbols: List[Symbol]
