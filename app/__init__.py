from fastapi import FastAPI

from app.configuration.server import Server


tags_meta_data = [
    {"name": "Pairs", "description": "Pairs endpoints"},
]


def create_app(_=None) -> FastAPI:
    app = FastAPI(
        title="Binance Cryptocurrency Parser",
        description="Simple edition",
        version="1.0",
        openapi_tags=tags_meta_data,
    )
    return Server(app).get_app()
