from app.configuration.routes.routes import Routes
from app.internal.routes import currency, user_check


__routes__ = Routes(routers=(user_check.router, currency.router))
