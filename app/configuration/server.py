import os
from dotenv import load_dotenv

from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from app.configuration.routes import __routes__
from app.internal.events.startup import on_loop_startup, on_startup


load_dotenv()


class Server:
    """
    Registrator.
    """

    __app: FastAPI

    def __init__(self, app: FastAPI):
        self.__app = app
        self.__register_events(app)
        self.__register_routes(app)

    def get_app(self) -> FastAPI:
        """
        Get the app method.
        """
        return self.__app

    @staticmethod
    def __register_events(app: FastAPI) -> None:
        """
        Events registration method.
        """

        app.on_event("startup")(repeat_every(seconds=60 * 60 * 12)(on_startup))
        app.on_event("startup")(
            repeat_every(
                seconds=float(os.getenv("FREQUENCY_REQUESTING_PAIRS_COST"))
            )(on_loop_startup)
        )

    @staticmethod
    def __register_routes(app: FastAPI) -> None:
        """
        Routes registration method.
        """
        __routes__.register_routes(app)
