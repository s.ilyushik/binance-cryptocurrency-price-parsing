@echo off
isort ./src/ %*
black ./src/ %*
flake8 %*
mypy ./src/ --show-error-codes --config-file setup.cfg %*
