#!/bin/bash

black .
pylint --rcfile=pylintrc ./app/*
isort .
flake8
mypy . --show-error-codes --config-file setup.cfg
